/*===================================
File Name    : notification.js
Description  : Notifications JS.
Author       : Bestwebcreator.
Version      : 1.7
===================================*/

var times = [5000, 8000 , 10000 , 6000, 11000, 9000];
var countries = ['AU','BR','CH','CO','DE','ES','EU','JP','SW','TK','UK','UR','US','VE'];


var themeInterval = setInterval('notification()', time());

function time() {
    return  times[Math.floor(Math.random()*6)] + 2000;
}
function notification() {
    spop({
        template: ' <div class="sale_notification d-flex align-items-center"> <div class="notification_inner"> <i class="ion-alert-circled"></i><div><h3>'+Math.floor(Math.random()*500000 + 3500)+' ETF <img src="assets/images/coin.png" height="25px"> </h3><p>Sold in <img src="assets/flags/'+countries[Math.floor(Math.random()*14)]+'.png" height="20px"> </p> </div> </div> </div>',
        group: 'submit-satus',
		style     : 'nav-fixed',// error or success
        position  : 'bottom-left',
        autoclose: 3000,
        icon: false
    });
    clearInterval(themeInterval);
    themeInterval = setInterval('notification()', time());
}